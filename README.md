# Gelbooru Browser
### Browse Gelbooru without annoying ads / manage your images and videos easier than ever.
###### (This is not an official project and it's not affiliated with Gelbooru in any way)

![Screenshot](http://i.imgur.com/Pa8XtRZ.png "Program screenshot")

## Precompiled Binaries

Requires .NET 4.5

[Precompiled](http://www.mediafire.com/file/0qc49x8n79kz41q/GelbooruBrowser.7z)

## Compile from source

Just download the source and use Visual Studio 2017 with C# to compile it.

## Manual (You must read it)

[Read the fcking manual](Docs/GelbooruBrowser_QuickGuidev1.pdf)

## Support Gelbooru

Despite the aggressive advertisings they have, Gelbooru is still awesome and I dont think
it's maintenance is cheap. If you use this software, please consider supporting it. They
have a Patreon for that:

[Gelbooru's Patreon](https://www.patreon.com/gelbooru)

## Buy me a pizza!

(Comming soon)
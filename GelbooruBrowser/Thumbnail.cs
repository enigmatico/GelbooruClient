﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */

using System;
using System.Runtime.Serialization;

/* Class that holds information about each image/video in the page */

namespace GelbooruBrowser
{
    [Serializable()]
    public class Thumbnail : ISerializable
    {
        /* ID of the image (read only) */
        private int id_;
        public int ID {
            get
            {
                return id_;
            }
        }

        //Thumbnail Image source (NOT THE IMAGE ITSELF!)
        public string src = "";
        //Link to the page of the image
        public string href = "";
        //Name of the thumbnail in the cache folder
        private string cache = "";
        //Image tags
        public string tags = "";
        //e q s
        public string rating = "";
        //
        public int thumbw = 0;
        public int thumbh = 0;
        public int score = 0;

        public DateTime searchdate = DateTime.Now;

        /* CONSTRUCTOR */
        public Thumbnail(int id, string source, string hyper, string cach, string tags_ = "", int thumbw_ = 100, int thumbh_ = 100, int score_ = 0, string rating_ = "e", string sf = "")
        {
            id_ = id;
            src = source;
            href = hyper;
            cache = cach;
            tags = tags_;
            thumbw = thumbw_;
            thumbh = thumbh_;
            score = score_;
            rating = rating_;
            searchdate = DateTime.Now;
        }

        public Thumbnail(SerializationInfo info, StreamingContext ctxt)
        {
            id_ = (int)info.GetValue("ID", typeof(int));
            src = (string)info.GetValue("src", typeof(string));
            href = (string)info.GetValue("href", typeof(string));
            cache = (string)info.GetValue("cache", typeof(string));
            tags = (string)info.GetValue("tags", typeof(string));
            score = (int)info.GetValue("score", typeof(int));
            rating = (string)info.GetValue("rating", typeof(string));
            searchdate = (DateTime)info.GetValue("searchdate", typeof(DateTime));
            thumbw = 0; //Unused
            thumbh = 0; //Unused
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ID", ID);
            info.AddValue("src", src);
            info.AddValue("href", href);
            info.AddValue("cache", cache);
            info.AddValue("tags", tags);
            info.AddValue("rating", rating);
            info.AddValue("score", score);
            info.AddValue("searchdate", searchdate);
        }
    }
}

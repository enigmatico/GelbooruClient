﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GelbooruBrowser
{
    public partial class WaitForm : Form
    {
        bool running = true;
        public WaitForm()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void WaitForm_Load(object sender, EventArgs e)
        {
            Task x = new Task(() => {
                int progress = 0;
                while(running)
                {
                    progress++;
                    if (progress > 100)
                        progress = 0;
                    progressBar1.Value = progress;
                    this.Refresh();
                    System.Threading.Thread.Sleep(10);
                }
            });
            x.Start();
        }

        private void WaitForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            running = false;
        }
    }
}

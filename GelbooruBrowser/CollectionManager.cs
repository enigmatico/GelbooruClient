﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using GelbooruBrowser;

namespace GelbooruBrowser
{
    public partial class CollectionManager : Form
    {
        string SuperRoot = "Gelbooru";
        CollectionBrowser cbr;
        Form1 parent;

        public CollectionManager(Form1 par)
        {
            InitializeComponent();
            parent = par;

        }

        /* Recursively scan trough each element in the collection root folder */
        private TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeNode(directoryInfo.Name);
            foreach (var directory in directoryInfo.GetDirectories())
                if(directory.Name != "meta")
                    directoryNode.Nodes.Add(CreateDirectoryNode(directory));
            return directoryNode;
        }

        /* This function is called to update the treeView with the current elements in the collection root folder */
        public void RescanSuperroot()
        {
            treeView1.Nodes.Clear();
            DirectoryInfo di = new DirectoryInfo(SuperRoot);
            treeView1.Nodes.Add(CreateDirectoryNode(di));
            treeView1.ExpandAll();  
        }

        /* Other forms can call this function to retrieve the current selected path */
        public string GetSelectedPath()
        {
            return treeView1.SelectedNode.FullPath;
        }

        public bool MakeCollection(string name, bool rescan)
        {
            TreeNode sel = treeView1.SelectedNode;
            if (System.IO.Directory.Exists(sel.FullPath + "\\" + name))
                return false;
            System.IO.Directory.CreateDirectory(sel.FullPath + "\\" + name);
            if(rescan)
                RescanSuperroot();
            return true;
        }

        public void RenameCollection(string name)
        {
            TreeNode sel = treeView1.SelectedNode;
            TreeNode parent = sel.Parent;
            System.IO.Directory.Move(sel.FullPath, parent.FullPath + "\\" + name);
            RescanSuperroot();
        }

        /* This function returns the path of the given collection. If more
         than one collection exists with that name, it will only return the
         first one. */
        //TODO: This might not be a good thing and needs to change.
        public string SearchCollectionPath(string Collection)
        {
            List<string> all = GetShortedCollections();
            foreach(string s in all)
            {
                string[] dirs = s.Split(new char[]{ '\\' });
                foreach (string ss in dirs)
                    if (ss == Collection)
                        return s;
            }
            return "";
        }

        /* This function is used by GetUnshortedCollections. It iterates trough all
         * the parent elements in a specified node, then returns the whole list of
         nodes. */
        public List<TreeNode> ScanNode(TreeNode tn)
        {
            if (tn == null)
                return new List<TreeNode>();
            List<TreeNode> result = new List<TreeNode>();
            result.Add(tn);
            TreeNodeCollection tnc = tn.Nodes;
            foreach (TreeNode tn2 in tnc)
                result.AddRange(ScanNode(tn2));
            return result;
        }

        /* This function returns an unsorted list with all the collections. No
         paths. */

        public List<string> GetUnshortedCollections()
        {
            List<string> result = new List<string>();
            List<TreeNode> nodelist = new List<TreeNode>();
            TreeNodeCollection tnc = treeView1.Nodes;
            foreach (TreeNode tn in tnc)
                nodelist.AddRange(ScanNode(tn));

            foreach (TreeNode tn2 in nodelist)
                result.Add(tn2.Text);
            return result;
        }

        /* Same as GetUnshortedCollections, but it returns a list of
         nodes with their full path, rather than just node names. */

        public List<string> GetShortedCollections()
        {
            List<string> result = new List<string>();
            List<TreeNode> nodelist = new List<TreeNode>();
            TreeNodeCollection tnc = treeView1.Nodes;
            foreach (TreeNode tn in tnc)
                nodelist.AddRange(ScanNode(tn));
            foreach (TreeNode tn2 in nodelist)
                result.Add(tn2.FullPath);
            return result;
        }

        /* Called when loaded */
        private void CollectionManager_Load(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(SuperRoot))
                System.IO.Directory.CreateDirectory(SuperRoot);

            RescanSuperroot();
            
        }

        private void newCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextPrompt tp = new TextPrompt("New Collection", "Name of the new collection:", this);
            tp.ShowDialog();
            return;
        }

        private void removeCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode sel = treeView1.SelectedNode;
            if (!System.IO.Directory.Exists(sel.FullPath))
            {
                RescanSuperroot();
                return;
            }

            if(sel.FullPath == SuperRoot)
            {
                MessageBox.Show("Root directory can not be deleted!");
                return;
            }

            DialogResult dr = MessageBox.Show("-- WARNING -- This will delete ALL DATA INSIDE THE SELECTED COLLECTION!!\n\nSelected collection: " + sel.FullPath + "\n\nARE YOU SURE ABOTU THIS? (It can't be undone!)",
                "WARNING!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes)
            {
                System.IO.Directory.Delete(sel.FullPath, true);
                RescanSuperroot();
                return;
            }
            else
                return;
        }

        private void renameCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode sel = treeView1.SelectedNode;
            if (!System.IO.Directory.Exists(sel.FullPath))
            {
                MessageBox.Show("Directory does not exist anymore.");
                RescanSuperroot();
                return;
            }
            if (sel.FullPath == SuperRoot)
            {
                MessageBox.Show("Root directory can not be renamed!");
                return;
            }
            TextPromptRename tp = new TextPromptRename("Rename Collection", "New name of the collection:", this);
            tp.ShowDialog();
            return;
        }

        private void exploreSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(cbr == null)
            {
                cbr = new CollectionBrowser(this);
                cbr.Show();
            }
            else
            {
                try
                {
                    cbr.Show();
                }catch(ObjectDisposedException ode)
                {
                    cbr = new CollectionBrowser(this);
                    cbr.Show();
                }
                cbr.Select();
            }

        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RescanSuperroot();
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (cbr != null)
                cbr.Reload();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            parent.setselcol(treeView1.SelectedNode.FullPath);
        }
    }
}

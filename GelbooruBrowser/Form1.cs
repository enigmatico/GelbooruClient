﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
//using HtmlAgilityPack;
using GelbooruBrowser;

namespace GelbooruBrowser
{
    public partial class Form1 : Form
    {
        private List<Thumbnail> thumbs;
        private const string uri = "https://gelbooru.com/index.php?page=post&s=list";
        private int page = 0;
        private int postsperpage = 42;
        private int ratelevel = 0;
        private Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]"); //Invalid characters for file names7paths

        public CollectionManager cm;

        public Form1()
        {
            InitializeComponent();
        }

        public void SerializeThumb(Thumbnail th, string fn)
        {
            if (!Directory.Exists(selcol.Text + "\\" + "meta"))
                Directory.CreateDirectory(selcol.Text + "\\" + "meta");
            string thname = selcol.Text + "\\meta\\" + fn + ".meta";
            Stream str = File.Open(thname, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(str, th);
            str.Close();
        }

        /* When selecting a collection in the collection manager,
         this function updates the displayed selected collection
         in Form1 for informative purposes. */
        public void setselcol(string colname)
        {
            selcol.Text = colname;
        }

        /* This function builds an URL that points to a specific gelbooru
         page */
        string makeuri(int page)
        {
            string tags = "";
            if(searchcrit.Text != "")
            {
                tags = searchcrit.Text.Replace(' ', '+');
            }
            string result = uri + "&pid=" + (page * postsperpage).ToString();
            if (tags != "")
                result += "&tags=" + tags;
            return result;
        }

        /* Called when Form1 is loaded */
        private void Form1_Load(object sender, EventArgs e)
        {
            thumbs = new List<Thumbnail>();
            cm = new CollectionManager(this);
            cm.Show();
            if (!Directory.Exists("cache"))
                Directory.CreateDirectory("cache");
            serv.SelectedIndex = 0;
        }

        /* Function that returns the thumbnail at index 'index' */
        Thumbnail SearchThumb(int index)
        {
            int x = 0;
            foreach(Thumbnail t in thumbs)
            {
                if (x == index)
                    return t;
                x++;
            }
            return null;
        }

        /* Function that downloads an HTML page from Gelbooru */
        string fetchpage()
        {
            string result = "";
            using (WebClient client = new WebClient())
            {
                string uri = makeuri(page);
                //MessageBox.Show("Downloading from " + uri);
                result = client.DownloadString(uri);
            }
            return result;
        }

        /* This function loads a thumbnail image */
        bool downloadImage(string uri, string dest)
        {
            if (uri.Contains("https:https"))
                uri = uri.Replace("https:https", "https");

            //MessageBox.Show("Downloading " + uri + " into " + dest);
            DirectoryInfo di = new FileInfo(dest + Path.GetExtension(uri)).Directory;
            if (!di.Exists)
                di.Create();
            using (WebClient client = new WebClient())
            {
                try {
                    if (!new FileInfo(dest + Path.GetExtension(uri)).Exists)
                        client.DownloadFile(uri, dest + Path.GetExtension(uri));
                }catch(Exception e)
                {
                    MessageBox.Show($"Error in downloadImage: {e.ToString()}");
                    return false;
                }
                
                if (new FileInfo(dest + Path.GetExtension(uri)).Exists)
                    return true;
                else
                    return false;
            }
        }

        //Clear the thumbail browser.
        private void ClearList()
        {
            imgl.Images.Clear();
            thumbdisp.Clear();
            thumbs.Clear();
        }

        private void LoadList()
        {
            WaitForm wf = new WaitForm();
            wf.Show();
            try
            {
                string servurl = (serv.SelectedItem.ToString() == "Gelbooru") ? "https://gelbooru.com" : 
                    (serv.SelectedItem.ToString() == "Safebooru") ? "https://safebooru.org" : 
                    (serv.SelectedItem.ToString() == "Sakugabooru") ? "https://sakugabooru.com" :
                    (serv.SelectedItem.ToString() == "Konachan") ? "https://konachan.com" : "";

                string search = searchcrit.Text;
                string html = string.Empty;
                string url = "";
                if (!string.IsNullOrEmpty(search))
                    url = $@"{servurl}/index.php?page=dapi&s=post&q=index&tags={search}&pid={page.ToString()}&limit={(int)PPP.Value}";
                else
                    url = $@"{servurl}/index.php?page=dapi&s=post&q=index&pid={page.ToString()}&limit={(int)PPP.Value}";

                if (url.Contains("sakugabooru") || url.Contains("konachan"))
                    url = url.Replace("index.php", "post.xml");

                //MessageBox.Show(url);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                XmlDocument results = new XmlDocument();
                results.LoadXml(html);

                XmlNode root = results.DocumentElement;
                if (root.Attributes["count"] == null)
                {
                    wf.Close();
                    wf.Dispose();
                    MessageBox.Show("Not found.");
                    return;
                }
                int posts = Convert.ToInt32(root.Attributes["count"]?.InnerText);
                if (posts < 1)
                {
                    wf.Close();
                    wf.Dispose();
                    MessageBox.Show("This query returns no results. Try another query.");
                    return;
                }
                //int randval = new Random().Next(0, posts - 1);
                for(int x = 0; x < root.ChildNodes.Count; x++)
                {
                    XmlNode selected = (XmlNode)root.ChildNodes[x];
                    int previeww = Convert.ToInt32(selected.Attributes["preview_width"]?.InnerText);
                    int previewh = Convert.ToInt32(selected.Attributes["preview_height"]?.InnerText);
                    int score = Convert.ToInt32(selected.Attributes["score"]?.InnerText);
                    int id = Convert.ToInt32(selected.Attributes["id"]?.InnerText);
                    string rating = selected.Attributes["rating"]?.InnerText;
                    string tags = selected.Attributes["tags"]?.InnerText;
                    string source = selected.Attributes["file_url"]?.InnerText.Replace("//", "https://");
                    string thumb = selected.Attributes["preview_url"]?.InnerText.Replace("//", "https://");

                    //MessageBox.Show($"{x.ToString()} :: {root.ChildNodes[x].Attributes["preview_url"].InnerText}");

                    string thname = "cache\\" + id.ToString();

                    int imgr = (rating == "e") ? 2 : (rating == "q") ? 1 : 0;

                    if((!ers.Checked && ratelevel >= imgr) || (ers.Checked && ratelevel == imgr))
                    {
                        if (downloadImage(thumb, thname))
                        {
                            thumbs.Add(new Thumbnail(id, thumb, source, thname, tags));
                            ListViewItem lvi = new ListViewItem();
                            lvi.ImageIndex = imgl.Images.Count;
                            FileStream fs = new FileStream(thname + Path.GetExtension(thumb), FileMode.Open, FileAccess.Read);
                            if (!fs.CanRead)
                            {
                                MessageBox.Show($"Error in FS for {thname.ToString()}: Is not readable.");
                                fs.Close();
                                continue;
                            }
                            imgl.Images.Add(Image.FromStream(fs));
                            fs.Close();
                            lvi.Tag = tags;
                            thumbdisp.Items.Add(lvi);
                            Thread.Sleep(1);
                        }
                    }

                }

                wf.Close();
                wf.Dispose();

            }
            catch(Exception exc)
            {
                wf.Close();
                wf.Dispose();
                MessageBox.Show("Error: " + exc.ToString());
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearList();
            page = (int)pagesel.Value;
            LoadList();
        }

        private void thumbdisp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (thumbdisp.SelectedItems.Count <= 0)
                return;
            Thumbnail t = SearchThumb(thumbdisp.SelectedIndices[0]);
            //tglist.Text = t.tags;
            string[] taglist = t.tags.Split(new char[] { ' ' });
            tglist2.Items.Clear();
            foreach (string s in taglist)
                tglist2.Items.Add(s);
            tglist2.SelectedIndex = 1;
            urlbox.Text = t.href;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClearList();
            page = (page<=1)?0:page-1;
            pagesel.Value = (Decimal)page;
            LoadList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClearList();
            page = (page >= (int)pagesel.Maximum - 1) ? (int)pagesel.Maximum - 1 : page+1;
            pagesel.Value = (Decimal)page;
            LoadList();
        }

        private void downloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (thumbdisp.SelectedItems.Count <= 0)
                return;
            string path = cm.GetSelectedPath();
            DirectoryInfo downloads = new DirectoryInfo(path);
            if (!downloads.Exists)
                downloads.Create();
            WaitForm wf = new WaitForm();
            wf.Show();
            for (int n = 0; n < thumbdisp.SelectedIndices.Count; n++)
            {
                Thumbnail t = SearchThumb(thumbdisp.SelectedIndices[n]);
                string fn = "";
                if (imgname1.Checked)
                    fn = t.ID.ToString();
                else if (imgname2.Checked)
                    fn = illegalInFileName.Replace(t.tags, "");
                else if (imgname3.Checked)
                    fn = DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString() + "-" + DateTime.Today.Year.ToString() + "-" +
                            DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();

                fn = fn.Replace(' ', '-');

                if (fn.Length > 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5))
                    fn = fn.Substring(0, 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5));

                string thname = selcol.Text + "\\" + fn;

                //MessageBox.Show($"Downloading {t.href} in {thname} for image index {n.ToString()}");
                downloadImage(t.href, thname);
                SerializeThumb(t, fn);
                //DownloadFullImage(t.href, t.ID, path, n);
            }
            wf.Close();
            wf.Dispose();
            MessageBox.Show("Downloaded " + thumbdisp.SelectedIndices.Count.ToString() + " images");
        }

        private void searchcrit_TextChanged(object sender, EventArgs e)
        {

        }

        private void downloadAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dres = MessageBox.Show("Downloading all images. Continue?", "Confirmation required", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dres == DialogResult.No)
                return;
            string path = cm.GetSelectedPath();
            DirectoryInfo downloads = new DirectoryInfo(path);
            if (!downloads.Exists)
                downloads.Create();
            WaitForm wf = new WaitForm();
            wf.Show();
            for (int n = 0; n < thumbdisp.Items.Count; n++)
            {
                Thumbnail t = SearchThumb(n);
                string fn = "";
                if (imgname1.Checked)
                    fn = t.ID.ToString();
                else if (imgname2.Checked)
                    fn = illegalInFileName.Replace(t.tags, "");
                else if (imgname3.Checked)
                    fn = DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString() + "-" + DateTime.Today.Year.ToString() + "-" +
                            DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();

                fn = fn.Replace(' ', '-');
                if (fn.Length > 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5))
                    fn = fn.Substring(0, 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5));

                string thname = selcol.Text + "\\" + fn;
                downloadImage(t.href, thname);
                SerializeThumb(t, fn);
                //DownloadFullImage(t.href, t.ID, path, n);
            }
            wf.Close();
            wf.Dispose();
            MessageBox.Show("Downloaded " + thumbdisp.Items.Count.ToString() + " images");
        }

        private bool isSelectedInList(int index)
        {
            foreach (int x in thumbdisp.SelectedIndices)
            {
                if (index == x)
                    return true;
            }
            return false;
        }

        private void downloadAllButSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dres = MessageBox.Show("Downloading " + (thumbdisp.Items.Count - thumbdisp.SelectedItems.Count).ToString() + " images. Continue?", "Confirmation required", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dres == DialogResult.No)
                return;
            string path = cm.GetSelectedPath();
            DirectoryInfo downloads = new DirectoryInfo(path);
            if (!downloads.Exists)
                downloads.Create();
            int downs = 0;
            WaitForm wf = new WaitForm();
            wf.Show();
            for (int n = 0; n < thumbdisp.Items.Count; n++)
            {
                if (isSelectedInList(n))
                    continue;
                Thumbnail t = SearchThumb(n);
                string fn = "";
                if (imgname1.Checked)
                    fn = t.ID.ToString();
                else if (imgname2.Checked)
                    fn = illegalInFileName.Replace(t.tags, "");
                else if (imgname3.Checked)
                    fn = DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString() + "-" + DateTime.Today.Year.ToString() + "-" +
                            DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();

                fn = fn.Replace(' ', '-');
                if (fn.Length > 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5))
                    fn = fn.Substring(0, 244 - (Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location).Length + selcol.Text.Length + 5));

                string thname = selcol.Text + "\\" + fn;
                downloadImage(t.href, thname);
                SerializeThumb(t, fn);
                //DownloadFullImage(t.href, t.ID, path, n);
                downs++;
            }
            wf.Close();
            wf.Dispose();
            MessageBox.Show("Downloaded " + downs.ToString() + " images");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                searchcrit.Text += " " + tglist2.SelectedItem.ToString();
            }
            catch(NullReferenceException nre)
            {

            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                searchcrit.Text += " -" + tglist2.SelectedItem.ToString();
            }
            catch (NullReferenceException nre)
            {

            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                searchcrit.Text = tglist2.SelectedItem.ToString();
            }
            catch (NullReferenceException nre)
            {

            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                cm.MakeCollection(tglist2.SelectedItem.ToString(), true);
            }
            catch (NullReferenceException nre)
            {

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (string s in tglist2.Items)
                    cm.MakeCollection(s, false);
                cm.RescanSuperroot();
            }catch(Exception exc)
            {

            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo("cache");
            foreach(FileInfo fi in di.GetFiles())
            {
                fi.Delete();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            urlbox.SelectAll();
            urlbox.Copy();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(urlbox.Text);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", "\"" + Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" + selcol.Text + "\"");
        }

        private void rs_CheckedChanged(object sender, EventArgs e)
        {
            if (rs.Checked)
                ratelevel = 0;
        }

        private void rq_CheckedChanged(object sender, EventArgs e)
        {
            if (rq.Checked)
                ratelevel = 1;
        }

        private void re_CheckedChanged(object sender, EventArgs e)
        {
            if (re.Checked)
                ratelevel = 2;
        }
    }
}

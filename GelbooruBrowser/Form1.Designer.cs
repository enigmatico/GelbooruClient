﻿namespace GelbooruBrowser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.thumbdisp = new System.Windows.Forms.ListView();
            this.cmenu1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.downloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadAllButSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imgl = new System.Windows.Forms.ImageList(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pagesel = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.searchcrit = new System.Windows.Forms.TextBox();
            this.tglist2 = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.urlbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.selcol = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgname3 = new System.Windows.Forms.RadioButton();
            this.imgname2 = new System.Windows.Forms.RadioButton();
            this.imgname1 = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.PPP = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.re = new System.Windows.Forms.RadioButton();
            this.rq = new System.Windows.Forms.RadioButton();
            this.rs = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.serv = new System.Windows.Forms.ComboBox();
            this.ers = new System.Windows.Forms.CheckBox();
            this.cmenu1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pagesel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PPP)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // thumbdisp
            // 
            this.thumbdisp.ContextMenuStrip = this.cmenu1;
            this.thumbdisp.LargeImageList = this.imgl;
            this.thumbdisp.Location = new System.Drawing.Point(16, 77);
            this.thumbdisp.Margin = new System.Windows.Forms.Padding(5);
            this.thumbdisp.Name = "thumbdisp";
            this.thumbdisp.Size = new System.Drawing.Size(656, 384);
            this.thumbdisp.TabIndex = 1;
            this.thumbdisp.UseCompatibleStateImageBehavior = false;
            this.thumbdisp.SelectedIndexChanged += new System.EventHandler(this.thumbdisp_SelectedIndexChanged);
            // 
            // cmenu1
            // 
            this.cmenu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadToolStripMenuItem,
            this.downloadAllToolStripMenuItem,
            this.downloadAllButSelectedToolStripMenuItem});
            this.cmenu1.Name = "cmenu1";
            this.cmenu1.Size = new System.Drawing.Size(211, 70);
            // 
            // downloadToolStripMenuItem
            // 
            this.downloadToolStripMenuItem.Name = "downloadToolStripMenuItem";
            this.downloadToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.downloadToolStripMenuItem.Text = "Download";
            this.downloadToolStripMenuItem.Click += new System.EventHandler(this.downloadToolStripMenuItem_Click);
            // 
            // downloadAllToolStripMenuItem
            // 
            this.downloadAllToolStripMenuItem.Name = "downloadAllToolStripMenuItem";
            this.downloadAllToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.downloadAllToolStripMenuItem.Text = "Download all";
            this.downloadAllToolStripMenuItem.Click += new System.EventHandler(this.downloadAllToolStripMenuItem_Click);
            // 
            // downloadAllButSelectedToolStripMenuItem
            // 
            this.downloadAllButSelectedToolStripMenuItem.Name = "downloadAllButSelectedToolStripMenuItem";
            this.downloadAllButSelectedToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.downloadAllButSelectedToolStripMenuItem.Text = "Download all but selected";
            this.downloadAllButSelectedToolStripMenuItem.Click += new System.EventHandler(this.downloadAllButSelectedToolStripMenuItem_Click);
            // 
            // imgl
            // 
            this.imgl.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imgl.ImageSize = new System.Drawing.Size(105, 150);
            this.imgl.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(564, 510);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "<";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(618, 510);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(50, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = ">";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Page:";
            // 
            // pagesel
            // 
            this.pagesel.Location = new System.Drawing.Point(134, 15);
            this.pagesel.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.pagesel.Name = "pagesel";
            this.pagesel.Size = new System.Drawing.Size(55, 20);
            this.pagesel.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(195, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Search:";
            // 
            // searchcrit
            // 
            this.searchcrit.Location = new System.Drawing.Point(245, 14);
            this.searchcrit.Name = "searchcrit";
            this.searchcrit.Size = new System.Drawing.Size(424, 20);
            this.searchcrit.TabIndex = 10;
            this.searchcrit.TextChanged += new System.EventHandler(this.searchcrit_TextChanged);
            // 
            // tglist2
            // 
            this.tglist2.FormattingEnabled = true;
            this.tglist2.Location = new System.Drawing.Point(52, 512);
            this.tglist2.Name = "tglist2";
            this.tglist2.Size = new System.Drawing.Size(506, 21);
            this.tglist2.TabIndex = 11;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 536);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(50, 23);
            this.button4.TabIndex = 12;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(68, 536);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(39, 23);
            this.button5.TabIndex = 13;
            this.button5.Text = "Filter";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(113, 536);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(61, 23);
            this.button6.TabIndex = 14;
            this.button6.Text = "Replace";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(225, 536);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(102, 23);
            this.button7.TabIndex = 15;
            this.button7.Text = "Gen Collection";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(333, 536);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(104, 23);
            this.button8.TabIndex = 16;
            this.button8.Text = "Generate all";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 568);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "URL:";
            // 
            // urlbox
            // 
            this.urlbox.Location = new System.Drawing.Point(50, 565);
            this.urlbox.Name = "urlbox";
            this.urlbox.ReadOnly = true;
            this.urlbox.Size = new System.Drawing.Size(508, 20);
            this.urlbox.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 515);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Tags:";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(564, 563);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(39, 23);
            this.button9.TabIndex = 22;
            this.button9.Text = "CPY";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(609, 563);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(42, 23);
            this.button10.TabIndex = 23;
            this.button10.Text = "BRW";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 470);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Selected collection:";
            // 
            // selcol
            // 
            this.selcol.AutoSize = true;
            this.selcol.Location = new System.Drawing.Point(114, 470);
            this.selcol.Name = "selcol";
            this.selcol.Size = new System.Drawing.Size(33, 13);
            this.selcol.TabIndex = 25;
            this.selcol.Text = "None";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(12, 486);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(60, 23);
            this.button11.TabIndex = 26;
            this.button11.Text = "Explorer";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "File names:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.imgname3);
            this.panel1.Controls.Add(this.imgname2);
            this.panel1.Controls.Add(this.imgname1);
            this.panel1.Location = new System.Drawing.Point(153, 486);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(220, 23);
            this.panel1.TabIndex = 28;
            // 
            // imgname3
            // 
            this.imgname3.AutoSize = true;
            this.imgname3.Location = new System.Drawing.Point(100, 3);
            this.imgname3.Name = "imgname3";
            this.imgname3.Size = new System.Drawing.Size(116, 17);
            this.imgname3.TabIndex = 2;
            this.imgname3.TabStop = true;
            this.imgname3.Text = "Download datetime";
            this.imgname3.UseVisualStyleBackColor = true;
            // 
            // imgname2
            // 
            this.imgname2.AutoSize = true;
            this.imgname2.Location = new System.Drawing.Point(45, 3);
            this.imgname2.Name = "imgname2";
            this.imgname2.Size = new System.Drawing.Size(49, 17);
            this.imgname2.TabIndex = 1;
            this.imgname2.Text = "Tags";
            this.imgname2.UseVisualStyleBackColor = true;
            // 
            // imgname1
            // 
            this.imgname1.AutoSize = true;
            this.imgname1.Checked = true;
            this.imgname1.Location = new System.Drawing.Point(3, 3);
            this.imgname1.Name = "imgname1";
            this.imgname1.Size = new System.Drawing.Size(36, 17);
            this.imgname1.TabIndex = 0;
            this.imgname1.TabStop = true;
            this.imgname1.Text = "ID";
            this.imgname1.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Posts Per Page:";
            // 
            // PPP
            // 
            this.PPP.Location = new System.Drawing.Point(96, 47);
            this.PPP.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PPP.Name = "PPP";
            this.PPP.Size = new System.Drawing.Size(55, 20);
            this.PPP.TabIndex = 30;
            this.PPP.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(157, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Rating:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.re);
            this.panel2.Controls.Add(this.rq);
            this.panel2.Controls.Add(this.rs);
            this.panel2.Location = new System.Drawing.Point(204, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(209, 20);
            this.panel2.TabIndex = 32;
            // 
            // re
            // 
            this.re.AutoSize = true;
            this.re.ForeColor = System.Drawing.Color.Red;
            this.re.Location = new System.Drawing.Point(150, 2);
            this.re.Name = "re";
            this.re.Size = new System.Drawing.Size(58, 17);
            this.re.TabIndex = 2;
            this.re.Text = "Explicit";
            this.re.UseVisualStyleBackColor = true;
            this.re.CheckedChanged += new System.EventHandler(this.re_CheckedChanged);
            // 
            // rq
            // 
            this.rq.AutoSize = true;
            this.rq.Location = new System.Drawing.Point(57, 2);
            this.rq.Name = "rq";
            this.rq.Size = new System.Drawing.Size(87, 17);
            this.rq.TabIndex = 1;
            this.rq.Text = "Questionable";
            this.rq.UseVisualStyleBackColor = true;
            this.rq.CheckedChanged += new System.EventHandler(this.rq_CheckedChanged);
            // 
            // rs
            // 
            this.rs.AutoSize = true;
            this.rs.Checked = true;
            this.rs.Location = new System.Drawing.Point(4, 2);
            this.rs.Name = "rs";
            this.rs.Size = new System.Drawing.Size(47, 17);
            this.rs.TabIndex = 0;
            this.rs.TabStop = true;
            this.rs.Text = "Safe";
            this.rs.UseVisualStyleBackColor = true;
            this.rs.CheckedChanged += new System.EventHandler(this.rs_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(495, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Service:";
            // 
            // serv
            // 
            this.serv.FormattingEnabled = true;
            this.serv.Items.AddRange(new object[] {
            "Gelbooru",
            "Safebooru",
            "Sakugabooru",
            "Konachan"});
            this.serv.Location = new System.Drawing.Point(544, 48);
            this.serv.Name = "serv";
            this.serv.Size = new System.Drawing.Size(121, 21);
            this.serv.TabIndex = 34;
            // 
            // ers
            // 
            this.ers.AutoSize = true;
            this.ers.Location = new System.Drawing.Point(418, 50);
            this.ers.Name = "ers";
            this.ers.Size = new System.Drawing.Size(71, 17);
            this.ers.TabIndex = 35;
            this.ers.Text = "Exclusive";
            this.ers.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 596);
            this.Controls.Add(this.ers);
            this.Controls.Add(this.serv);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.PPP);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.selcol);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.urlbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.tglist2);
            this.Controls.Add(this.searchcrit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pagesel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.thumbdisp);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Multibooru Browser +";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.cmenu1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pagesel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PPP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView thumbdisp;
        private System.Windows.Forms.ImageList imgl;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown pagesel;
        private System.Windows.Forms.ContextMenuStrip cmenu1;
        private System.Windows.Forms.ToolStripMenuItem downloadToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox searchcrit;
        private System.Windows.Forms.ToolStripMenuItem downloadAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downloadAllButSelectedToolStripMenuItem;
        private System.Windows.Forms.ComboBox tglist2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox urlbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label selcol;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton imgname3;
        private System.Windows.Forms.RadioButton imgname2;
        private System.Windows.Forms.RadioButton imgname1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown PPP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton re;
        private System.Windows.Forms.RadioButton rq;
        private System.Windows.Forms.RadioButton rs;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox serv;
        private System.Windows.Forms.CheckBox ers;
    }
}


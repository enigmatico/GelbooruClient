﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using GelbooruBrowser;

namespace GelbooruBrowser
{
    public partial class CollectionBrowser : Form
    {
        private CollectionManager cm;
        private string[] extensions = { "jpg", "jpeg", "png", "bmp", "webp" };
        private string wpath = "";
        private string fpath = "";
        private List<Thumbnail> thumbs;

        public CollectionBrowser(CollectionManager cm_)
        {
            InitializeComponent();
            cm = cm_;
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        /* Can be called to reload the folder */
        public void Reload()
        {
            thumbs.Clear();
            listView1.Clear();
            wpath = cm.GetSelectedPath();
            fpath = wpath += "\\";
            this.Text += wpath + " (Collection browser)";
            listView1.Clear();
            if (!String.IsNullOrEmpty(wpath))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(wpath);
                foreach (FileInfo f in directoryInfo.GetFiles())
                {
                    if (extensions.Any(f.Extension.Contains))
                    {
                        FileStream iss = new FileStream(f.FullName, FileMode.Open, FileAccess.Read);
                        Image img = Image.FromStream(iss);
                        imageList1.Images.Add(img);
                        iss.Close();
                        listView1.Items.Add(f.Name, imageList1.Images.Count - 1);
                    }
                }
                comboBox1.Items.Clear();
                List<string> AllNodes = cm.GetUnshortedCollections();
                foreach (string s in AllNodes)
                    comboBox1.Items.Add(s);
                comboBox1.SelectedIndex = 1;
            }
            else { return; }
        }

        private void CollectionBrowser_Load(object sender, EventArgs e)
        {
            thumbs = new List<Thumbnail>();
            Reload();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;
            int proc = 0;
            foreach (ListViewItem lvi in listView1.SelectedItems)
            {
                string origin = fpath + lvi.Text;
                string destination = cm.SearchCollectionPath(comboBox1.SelectedItem.ToString());
                if(destination != "")
                {
                    System.IO.File.Copy(origin, destination + "\\" + lvi.Text);
                    proc++;
                }
            }
            MessageBox.Show(proc.ToString() + " files copied to " + comboBox1.SelectedItem.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;
            int proc = 0;
            foreach (ListViewItem lvi in listView1.SelectedItems)
            {
                string origin = fpath + lvi.Text;
                string destination = cm.SearchCollectionPath(comboBox1.SelectedItem.ToString());
                if (destination != "")
                {
                    System.IO.File.Move(origin, destination + "\\" + lvi.Text);
                    proc++;
                }
            }
            MessageBox.Show(proc.ToString() + " files moved to " + comboBox1.SelectedItem.ToString());
            Reload();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;
            labelFullPath.Text = fpath + listView1.SelectedItems[0].Text;
            labelExtension.Text = new FileInfo(listView1.SelectedItems[0].Text).Extension;

            string metapath = wpath + "\\meta\\" + Path.GetFileNameWithoutExtension(listView1.SelectedItems[0].Text) + ".meta";
            Thumbnail th;
            Stream fs = new FileStream(metapath, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            th = (Thumbnail)bf.Deserialize(fs);
            fs.Close();

            idlabel.Text = th.ID.ToString();
            metatags.Text = th.tags;
            metasource.Text = th.src;
            metarating.Text = th.rating;

            foreach (ListViewItem i in listView1.SelectedItems)
                i.EnsureVisible();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;
            DialogResult dr = MessageBox.Show("-- WARNING -- This will delete ALL SELECTED IMAGES PERMANENTLY!!\n\nARE YOU SURE ABOUT THIS? (It can't be undone!)",
    "WARNING!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes)
            {
                int proc = 0;
                foreach (ListViewItem lvi in listView1.SelectedItems)
                {
                    string origin = fpath + lvi.Text;
                    System.IO.File.Delete(origin);
                    proc++;
                }
                MessageBox.Show(proc.ToString() + " files were deleted");
                Reload();
                return;
            }
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void op_AddIterationNumber_CheckedChanged(object sender, EventArgs e)
        {
            panel2.Enabled = !panel2.Enabled;
        }

        //RenameMode: all possible renaming options.
        enum RenameMode:int {
            NoChange = 0,
            Replace,
            Before,
            After
        };
        //IterationMode: all possible iteration options.
        enum IterationMode : int
        {
            Replace = 0,
            Before,
            After
        };
        private RenameMode rmode = RenameMode.NoChange;
        private IterationMode imode = IterationMode.Replace;

        private void button6_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nametext.Text))
                return;

            if (radioButton1.Checked)
                rmode = RenameMode.Replace;
            else if (radioButton2.Checked)
                rmode = RenameMode.Before;
            else if (radioButton3.Checked)
                rmode = RenameMode.After;
            else
                rmode = RenameMode.NoChange;

            bool useIM = op_AddIterationNumber.Checked;

            if (useIM)
                if (radioButton4.Checked)
                    imode = IterationMode.Replace;
                else if (radioButton5.Checked)
                    imode = IterationMode.Before;
                else
                    imode = IterationMode.After;

            int sc = (int)numericUpDown1.Value;
            string nstr = nametext.Text;

            int n = sc;
            string temp = "";
            foreach(ListViewItem i in listView1.SelectedItems)
            {
                FileInfo fi = new FileInfo(i.Text);
                string input = fpath + i.Text;
                string ntxt = nametext.Text;
                string output = fpath;
                string nout = "";
                if(rmode != RenameMode.NoChange)
                    switch(rmode)
                    {
                        case RenameMode.Replace:
                            nout = ntxt;
                            break;
                        case RenameMode.Before:
                            nout = ntxt + Path.GetFileNameWithoutExtension(fi.FullName);
                            break;
                        case RenameMode.After:
                            nout = Path.GetFileNameWithoutExtension(fi.FullName) + ntxt;
                            break;
                        case RenameMode.NoChange:
                            nout = Path.GetFileNameWithoutExtension(fi.FullName);
                            break;
                    }
                if(useIM)
                    switch(imode)
                    {
                        case IterationMode.Before:
                            temp = n.ToString() + nout;
                            nout = temp;
                            break;
                        case IterationMode.After:
                            temp = nout + n.ToString();
                            nout = temp;
                            break;
                        case IterationMode.Replace:
                            temp = n.ToString();
                            nout = temp;
                            break;
                    }

                output += nout + fi.Extension;
                if(!File.Exists(output))
                {
                    File.Move(input, output);
                    n++;
                }
            }
            MessageBox.Show("Renamed " + (n - sc).ToString() + " out of " + listView1.SelectedItems.Count.ToString() + " files!");
            Reload();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in listView1.Items)
                i.Selected = true;
            foreach (ListViewItem i in listView1.SelectedItems)
                i.EnsureVisible();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem i in listView1.SelectedItems)
                i.Selected = false;
        }

        private void showInExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
                return;
            
            System.Diagnostics.Process.Start("explorer.exe", "/select, \"" + Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\" + cm.GetSelectedPath() + "\\" + listView1.SelectedItems[0].Text + "\"");
        }
    }
}

﻿/* THIS WHOLE PROGRAM WAS CODED BY 3nigma. 
 PLEASE INVITE ME TO A PIZZA IF YOU LIKE IT. */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GelbooruBrowser;

namespace GelbooruBrowser
{
    public partial class TextPromptRename : Form
    {
        private CollectionManager cm;
        public TextPromptRename(string title, string prompt, CollectionManager cm_)
        {
            InitializeComponent();
            this.Text = title;
            label1.Text = prompt;
            cm = cm_;
        }

        private void TextPromptRename_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("Please enter a name for the collection.");
                return;
            }
            cm.RenameCollection(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
